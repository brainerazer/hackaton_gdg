from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required

from models import *

from django.contrib.auth import logout
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.contrib.auth.forms import UserCreationForm

def index(request):
    tasks = Task.objects.all().order_by('-added')[:3]
    form = UserCreationForm()
    context = {
        'task_list': tasks,
        'form': form
    }
    return render(request, 'index.html', context)

def task(request, task_id):
    task = Task.objects.get(pk=task_id)

    subtasks = task.subtasks.all().order_by('number')
    videos = task.video_set.all()
    photos = task.photo_set.all()

    sub_count = task.subtasks.count()
    completed = task.subtasks.filter(is_done=True).count()

    subscribed = task.subscribers.all()

    form = SubtaskForm()

    context = {
        'task': task,
        'subtasks': subtasks,
        'videos': videos,
        'photos': photos,
        'sub_count': sub_count,
        'completed': completed,
        'subscribed': subscribed,
        'form': form
    }

    return render(request, 'task.html', context)

def login(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            auth_login(request, user)
            # Redirect to a success page.
    return redirect('/')

def logout_view(request):
    logout(request)
    return redirect('/')

def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            new_user = form.save()
    return redirect("/")

def add_task(request):
    if request.POST:
        data = request.POST
        form = TaskForm(data, request.FILES)
        if form.is_valid():
            task = Task()
            task.owner = request.user
            task.name = form.cleaned_data['name']
            task.descr = form.cleaned_data['descr']
            task.image = form.cleaned_data['image']
            task.deadline = form.cleaned_data['deadline']
            task.lat = form.cleaned_data['lat']
            task.lon = form.cleaned_data['lon']

            task.save()

            return redirect('/')
        return render(request, 'one.html', {'form':form})


    else:
        form = TaskForm()

    return render(request, 'one.html', {'form':form})

def add_subtask(request, task_id):
    data = request.POST
    form = SubtaskForm(data)
    task = Task.objects.get(pk=task_id)

    subtasks = task.subtasks.all().order_by('-number')
    if not subtasks:
        n = 1
    else:
        n = subtasks[0].number

    if form.is_valid():
        stask = Subtask()
        stask.name = form.cleaned_data['name']
        stask.descr = form.cleaned_data['descr']
        stask.task = task
        stask.is_done = False
        stask.number = n + 1


        stask.save()

    return redirect('/tasks/' + task_id + '/')



@login_required
def list_all(request):
    tasks = Task.objects.all().order_by('-added')
    return render(request, 'all.html', {'task_list': tasks})

@login_required
def list_my(request):
    tasks = Task.objects.filter(owner=request.user).order_by('-added')
    return render(request, 'all.html', {'task_list': tasks})
