from django.db import models
from django.contrib.auth.models import User

from django_google_maps import fields as map_fields
from django import forms

def upload_image_to(instance, filename):
    import os
    from django.utils.timezone import now
    filename_base, filename_ext = os.path.splitext(filename)
    return 'tasks/%s%s' % (
        now().strftime("%Y%m%d%H%M%S"),
        filename_ext.lower(),
    )

class Task(models.Model):
    name = models.CharField(max_length=200)
    descr = models.TextField()
    deadline = models.DateField()
    added = models.DateTimeField(auto_now_add=True)
    owner = models.ForeignKey(User)
    image = models.ImageField("Image", upload_to=upload_image_to)

    subscribers = models.ManyToManyField(User, related_name="tasks")

    lat = models.CharField('Latitude', max_length=100)
    lon = models.CharField('Longitude', max_length=100)


class TaskForm(forms.Form):
    name = forms.CharField()
    descr = forms.CharField(widget=forms.Textarea)
    deadline = forms.DateField()
    image = forms.ImageField()

    lat = forms.CharField()
    lon = forms.CharField()

class SubtaskForm(forms.Form):
    name = forms.CharField()
    descr = forms.CharField(widget=forms.Textarea)

class Video(models.Model):
    data = models.FileField(upload_to=upload_image_to)
    task = models.ForeignKey(Task)

class Photo(models.Model):
    data = models.ImageField(upload_to=upload_image_to)
    task = models.ForeignKey(Task)

class Subtask(models.Model):
    name = models.CharField(max_length=200)
    descr = models.TextField()
    number = models.IntegerField()
    task = models.ForeignKey(Task, related_name='subtasks')
    is_done = models.BooleanField()