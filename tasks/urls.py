from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

import views

from rest_framework import viewsets,routers
from serializers import *


class TaskViewSet(viewsets.ModelViewSet):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer


router = routers.DefaultRouter()
router.register(r'tasks', TaskViewSet)
router.register(r'users', UserViewSet)

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'tasks.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', views.index, name='Index'),
    url(r'^tasks/(?P<task_id>\d+)', views.task, name='Task'),
    url(r'^api/', include(router.urls)),
    url(r'^logout/', views.logout_view),
    url(r'^api/api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'', include('social_auth.urls')),
    url(r'^avatar/', include('avatar.urls')),
    url(r'^login/', views.login),
    url(r'^register/', views.register),
    url(r'^add/', views.add_task),
    url(r'^list/', views.list_all),
    url(r'^my/', views.list_my),
    url(r'^subtasks/add/(?P<task_id>\d+)', views.add_subtask),


)
