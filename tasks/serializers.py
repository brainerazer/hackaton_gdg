from rest_framework import serializers
from models import *
from django.contrib.auth.models import User

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email')

class SubtaskSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Subtask
        fields = ('name', 'descr', 'number', 'is_done', 'task')


class PhotoSerializer(serializers.HyperlinkedModelSerializer):
    data = serializers.Field('data.url')
    class Meta:
        model = Photo
        fields = ('data',)


class VideoSerializer(serializers.HyperlinkedModelSerializer):
    data = serializers.Field('data.url')
    class Meta:
        model = Video
        fields = ('data',)


class TaskSerializer(serializers.HyperlinkedModelSerializer):
    subtasks = SubtaskSerializer()
    photo_set = PhotoSerializer()
    video_set = VideoSerializer()
    owner = UserSerializer()
    image = serializers.Field('image.url')
    class Meta:
        model = Task
        fields = ('name', 'descr', 'deadline', 'added', 'owner', 'subtasks', 'image', 'lat', 'lon',
            'photo_set', 'video_set')


