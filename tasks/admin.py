from django.contrib import admin
from tasks.models import *

from django_google_maps import widgets as map_widgets

class TaskAdmin(admin.ModelAdmin):
    formfield_overrides = {
        map_fields.AddressField: {'widget': map_widgets.GoogleMapsAddressWidget},
    }

admin.site.register(Task, TaskAdmin)
admin.site.register(Photo)
admin.site.register(Video)
admin.site.register(Subtask)